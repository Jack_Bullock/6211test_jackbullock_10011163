﻿/*------------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------Question 2 ------------------------------------------------------*/
/*---------------------------------Learning outcomes 1 & 2 - Data Types and Recurssion------------------------------------*/
/*----------------------------------------------------Marks - Out of 10----------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question2
{
/*------------------------------------------------------------------------------------------------------------------------*/
/*-----------Question 2 - Implement a recursive method to find a factorial using hardcoded and user input numbers---------*/
/*------------------------------------------------------Marks - Out of 10-------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/
    class Program
    {
        static void Main(string[] args)
        /*Hardcoded and user input passed to the factorial recursive method(Marks out of 5)*/
        {
            Console.WriteLine("*******************************************************");
            Console.WriteLine("**                                                   **");
            Console.WriteLine("**                    Question 2                     **");
            Console.WriteLine("**                                                   **");
            Console.WriteLine("*******************************************************");
            int answer = 0;
            answer = FindFactorial(10);
            Console.WriteLine("10 Factorial is : " + answer);
            Console.WriteLine("Please enter a number");
            int fact = 0;
            fact = int.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("*******************************************************");
            Console.WriteLine("**                                                   **");
            Console.WriteLine("**                    Question 2                     **");
            Console.WriteLine("**                                                   **");
            Console.WriteLine("*******************************************************");
            Console.WriteLine("10 Factorial is : " + answer);
            Console.WriteLine($"Please enter a number [ {fact}]");
            int answer2 = 0;
            answer2 = FindFactorial(fact);
            Console.WriteLine($"{fact} Factorial is : " + answer2);
            Console.WriteLine("*******************************************************");
            Console.WriteLine("**                                                   **");
            Console.WriteLine("**          Press ENTER to exit the app              **");
            Console.WriteLine("**                                                   **");
            Console.WriteLine("*******************************************************");
            Console.ReadKey();
            bool running = true;
            while (running) { };
            Console.ReadKey();


        }

        private static int FindFactorial(int factorialNum)
        {
            if (factorialNum == 1)
                return 1;
            return factorialNum * FindFactorial(factorialNum - 1);
            
        }
    }

/*------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------Question 2 END-------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/
}


